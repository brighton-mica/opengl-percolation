#include "Mesh.h"

unsigned int Mesh::meshId{ 0 };

Mesh::Mesh() 
{
	// std::cout << "Mesh " << meshId << " constructor" << std::endl;
	meshId++;
	VAO = 0;
	VBO = 0;
	IBO = 0;
	indexCount = 0;
	glPrimitive = 0;
}

Mesh::~Mesh()
{
	meshId--;
	// std::cout << "Mesh " << meshId << " destructor" << std::endl;
	ClearMesh();
}

void Mesh::CreateMesh(GLfloat* vertices, unsigned int* indices, unsigned int numOfVertices, unsigned int numOfIndices, GLenum primitive) {
	indexCount = numOfIndices;
	glPrimitive = primitive;

	// Create VAO on graphics card
	// 1 - we want to create 1 array
	// &VAO - where we want to store the ID of the array created on the graphics card
	glGenVertexArrays(1, &VAO);

	// Any OpenGL fucnction that interacts with interacts with VBOs will be using using the vertex
	// array on the graphics card defined on the CPU by VAO
	glBindVertexArray(VAO);

	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * numOfIndices, indices, GL_STATIC_DRAW);

	// Create buffer object inside VAO
	// 1 - we want to create 1 buffer
	// &VBO - where we want to store the ID of the buffer created on the graphics card
	glGenBuffers(1, &VBO);

	// Bind the buffer
	// GL_ARRAY_BUFFER - vertex attributes
	// VBO - the id of the buffer we want to bind
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	// Input buffer data (vertices) into VBO
	// GL_STATIC_DRAW - the data will be modified once and used many times as the source for GL drawing commands
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * numOfVertices, vertices, GL_STATIC_DRAW);

	// (index, size, type, normalized, stride, pointer)
	// index - the index of the generic vertex attribute to be modified
	// size - the number of components per vertex attribute (x, y, z)
	// type - the data of each component in the array
	// stride - the byte offset between generic vertex attributes 
	// pointer - offset of the first component of the first generic vertex attribute in the array in the data store of the buffer currently bound to the GL_ARRAY_BUFFER target
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// Enable the usage of the atribute "arg" in the shader
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Mesh::RenderMesh() {
	glBindVertexArray(VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glDrawElements(glPrimitive, indexCount, GL_UNSIGNED_INT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Mesh::ClearMesh() {
	if (IBO != 0) {
		glDeleteBuffers(1, &IBO);
		IBO = 0;
	}
	if (VBO != 0) {
		glDeleteBuffers(1, &VBO);
		VBO = 0;
	}
	if (VAO != 0) {
		glDeleteVertexArrays(1, &VAO);
		VAO = 0;
	}

	indexCount = 0;
	glPrimitive = 0;
}