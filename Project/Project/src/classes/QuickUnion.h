#pragma once
#include <vector>

class QuickUnion
{
    public:
        QuickUnion(const int N);
        void Union(const int A, const int B);
        bool Connected(const int A, const int B);

        std::vector<int> GetForest() { return forest; }
        std::vector<int> GetSize() { return size; }
        int GetRoot(int i) { return Root(i); }

    private:
        int Root(int i);
        std::vector<int> forest;
        std::vector<int> size;
};