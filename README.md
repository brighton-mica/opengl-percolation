![Percolation (Quick Union) Project](Percolation.gif)

This project uses the quick union algorithm to determine when two nodes are
connected, e.g., when there is a path from one end of the rectangle to the 
other.

Black - blocked off path<br>
White - open path<br>
Blue - paths reachable from the top of the rectangle<br>

The simulation can be adjusted with sizes ranging from a 3x3 to a 75x75.