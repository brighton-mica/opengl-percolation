#include <iostream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <unordered_set>
#include <stack>

#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "IMGUI/imgui.h"
#include "IMGUI/imgui_impl_glfw.h"
#include "IMGUI/imgui_impl_opengl3.h"
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "classes/Window.h"
#include "classes/Mesh.h"
#include "classes/Shader.h"
#include "classes/QuickUnion.h"

static const GLint WINDOW_WIDTH = 800;
static const GLint WINDOW_HEIGHT = 600;

static int gridDim = 3;
static int gridDimSqr = gridDim * gridDim;
static float pValue = 0.0; // sites are vacant with a probability of p
static std::vector<std::unique_ptr<Mesh>> meshTable;
static std::vector<int> vacant;
static std::vector<int> occupied;
static std::stack<int> history;
static std::vector<bool> stateTable(false);
static QuickUnion percolation = QuickUnion(gridDim * gridDim + 2); // gridDimSqr = top vNode, gridDimSqr + 1 = bor vNode

bool contains(std::vector<int>& vec, int n)
{
	return std::find(vec.begin(), vec.end(), n) != vec.end();
}

void GenerateGrid()
{
	float segment = 1.0f / gridDim;
	for (int row = 0; row < gridDim; row++)
	{
		float y = row / (gridDim * 1.0f);
		for (int col = 0; col < gridDim; col++)
		{
			float x = col / (gridDim * 1.0f);

			GLfloat vertices[] = {
				x, y, 0.0f,							// bot left
				x + segment, y, 0.0f,				// bot right
				x + segment, y + segment, 0.0f,		// top right
				x, y + segment, 0.0f				// top left
			};

			unsigned int indices[] = {
				0, 1, 2, 0, 3, 2
			};

			std::unique_ptr<Mesh> square = std::make_unique<Mesh>();
			square->CreateMesh(vertices, indices, 12, 6, GL_TRIANGLES);
			meshTable.push_back(std::move(square));
		}
	}
}

void GenerateShaders(std::vector<std::unique_ptr<Shader>> &shaderList)
{
	std::unique_ptr<Shader> shader = std::make_unique<Shader>();
	shader->CreateFromFiles("src/shaders/shader.vert", "src/shaders/shader.frag");
	shaderList.push_back(std::move(shader));
}

void UpdateState()
{

	while ((vacant.size() / (gridDimSqr * 1.0f)) < pValue)
	{


		// get a random index in occupied
		int randNum = rand() % occupied.size();
		int site = occupied[randNum];

		// delete random index in ocupied and move value at index to vacant
		occupied.erase(occupied.begin() + randNum);
		vacant.push_back(site);

		// std::cout << site << std::endl;

		if (site < gridDim)
		{
			if (site == 0)
			{
				//std::cout << "B1" << std::endl;
				if (contains(vacant, 1))
					percolation.Union(0, 1);
				if (contains(vacant, gridDim))
					percolation.Union(0, gridDim);
			}
			else if (site == gridDim - 1)
			{
				//std::cout << "B2" << std::endl;
				if (contains(vacant, site - 1))
					percolation.Union(site, site - 1);
				if (contains(vacant, site + gridDim))
					percolation.Union(site, site + gridDim);
			}
			else
			{
				//std::cout << "B3" << std::endl;
				if (contains(vacant, site + 1))
					percolation.Union(site, site + 1);
				if (contains(vacant, site - 1))
					percolation.Union(site, site - 1);
				if (contains(vacant, site + gridDim))
					percolation.Union(site, site + gridDim);
			}
		}
		else if (site < gridDimSqr - gridDim)
		{
			if (site % gridDim == 0)
			{
				//std::cout << "A1" << std::endl;
				if (contains(vacant, site + 1))
					percolation.Union(site, site + 1);
				if (contains(vacant, site - gridDim))
					percolation.Union(site, site - gridDim);
				if (contains(vacant, site + gridDim))
					percolation.Union(site, site + gridDim);
			}
			else if (site % gridDim == (gridDim - 1))
			{
				//std::cout << "A2" << std::endl;
				if (contains(vacant, site - 1))
					percolation.Union(site, site - 1);
				if (contains(vacant, site - gridDim))
					percolation.Union(site, site - gridDim);
				if (contains(vacant, site + gridDim))
					percolation.Union(site, site + gridDim);
			}
			else
			{
				//std::cout << "A3" << std::endl;
				if (contains(vacant, site + 1))
					percolation.Union(site, site + 1);
				if (contains(vacant, site - 1))
					percolation.Union(site, site - 1);
				if (contains(vacant, site - gridDim))
					percolation.Union(site, site - gridDim);
				if (contains(vacant, site + gridDim))
					percolation.Union(site, site + gridDim);
			}
		}
		else
		{
			if (site % gridDim == 0)
			{
				//std::cout << "C1" << std::endl;
				if (contains(vacant, site + 1))
					percolation.Union(site, site + 1);
				if (contains(vacant, site - gridDim))
					percolation.Union(site, site - gridDim);
			}
			else if (site % gridDim == (gridDim - 1))
			{
				//std::cout << "C2" << std::endl;
				if (contains(vacant, site - 1))
					percolation.Union(site, site - 1);
				if (contains(vacant, site - gridDim))
					percolation.Union(site, site - gridDim);
			}
			else
			{
				//std::cout << "C3" << std::endl;
				if (contains(vacant, site + 1))
					percolation.Union(site, site + 1);
				if (contains(vacant, site - 1))
					percolation.Union(site, site - 1);
				if (contains(vacant, site - gridDim))
					percolation.Union(site, site - gridDim);
			}
		}
		/*for (auto i : percolation.GetForest())
			std::cout << i << " ";
		std::cout << std::endl;*/
	}
}

int main() {
	Window window = Window(WINDOW_WIDTH, WINDOW_HEIGHT);
	if (!window.init()) { return 1; }

	// Setup Dear ImGui context
	{
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		ImGui::StyleColorsDark();
		ImGui_ImplGlfw_InitForOpenGL(window.getWindow(), true);
		ImGui_ImplOpenGL3_Init((char*)glGetString(GL_NUM_SHADING_LANGUAGE_VERSIONS));
	}

	// Program Init
	GLuint uniformModel = 0, uniformColor = 0;
	std::vector<std::unique_ptr<Shader>> shaders;
	GenerateShaders(shaders);
	for (int i = 0; i < gridDimSqr; i++)
		occupied.push_back(i);
	GenerateGrid();
	for (int i = 0; i < gridDim; i++)
		percolation.Union(gridDimSqr + 1, i);
	for (int i = gridDimSqr - gridDim; i < gridDimSqr; i++)
		percolation.Union(gridDimSqr, i);
	bool simulationRunning = false;
	enum simulationState { RUNNING, PAUSED, OVER, START } state;
	state = START;

	while (!window.shouldClose())
	{
		glfwPollEvents();
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (percolation.Connected(gridDimSqr, gridDimSqr + 1))
			state = OVER;

		// Render Mesh
		{
			shaders[0]->UseShader();
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(-0.5f, -0.5f, 0.0f));
			model = glm::scale(model, glm::vec3(1.2f, 1.2f, 1.0f));
			uniformModel = shaders[0]->GetModelLocation();
			glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
			uniformColor = shaders[0]->GetColorLocation();

			// Vacant rendering
			
			for (int i : vacant)
			{
				if (percolation.Connected(gridDimSqr, i))
				{
					glUniform3f(uniformColor, 0.52f, 0.81f, 0.98f);
				}
				else
				{
					glUniform3f(uniformColor, 1.0f, 1.0f, 1.0f);
				}
				meshTable[i]->RenderMesh();
			}

			// Occupied rendering
			glUniform3f(uniformColor, 0.0f, 0.0f, 0.0f);
			for (int i : occupied)
			{
				meshTable[i]->RenderMesh();
			}
			glUseProgram(0);
		}

		// Dear ImGui
		{
			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();
			ImGui::Begin("Dynamic Connectivity with Percolation");

			// Grid Dimension Slider
			{
				{
					ImGui::Text("P-value: %f", pValue);
				}
				if (state != OVER)
				{
					int n = gridDim;

					ImGui::SetNextItemWidth(100.0f);
					ImGui::SliderInt("Grid Dimension", &gridDim, 3, 75);
					if (n != gridDim)
					{
						if (state == OVER) pValue = 0.0f;
						gridDimSqr = gridDim * gridDim;
						meshTable.clear();
						vacant.clear();
						occupied.clear();
						for (int i = 0; i < gridDimSqr; i++)
							occupied.push_back(i);

						// Clear percolation
						percolation = QuickUnion(gridDimSqr + 2);
						for (int i = 0; i < gridDim; i++)
							percolation.Union(gridDimSqr + 1, i);
						for (int i = gridDimSqr - gridDim; i < gridDimSqr; i++)
							percolation.Union(gridDimSqr, i);

						GenerateGrid();
						UpdateState();
					}
				}
			}

			// Simulation State Manager
			{
				if (state == START)
				{
					if (ImGui::Button("Run Simulation"))
						state = RUNNING;
				}
				else if (state == RUNNING)
				{
					if (ImGui::Button("Pause Simulation"))
						state = PAUSED;
					else if (ImGui::Button("Reset Simulation"))
					{
						state = START;
						pValue = 0.0f;
						vacant.clear();
						occupied.clear();
						for (int i = 0; i < gridDimSqr; i++)
							occupied.push_back(i);
						percolation = QuickUnion(gridDimSqr + 2);
						for (int i = 0; i < gridDim; i++)
							percolation.Union(gridDimSqr + 1, i);
						for (int i = gridDimSqr - gridDim; i < gridDimSqr; i++)
							percolation.Union(gridDimSqr, i);
					}
				}
				else if (state == PAUSED)
				{
					if (ImGui::Button("Resume Simulation"))
						state = RUNNING;
					else if (ImGui::Button("Reset Simulation"))
					{
						state = START;
						pValue = 0.0f;
						vacant.clear();
						occupied.clear();
						for (int i = 0; i < gridDimSqr; i++)
							occupied.push_back(i);
						percolation = QuickUnion(gridDimSqr + 2);
						for (int i = 0; i < gridDim; i++)
							percolation.Union(gridDimSqr + 1, i);
						for (int i = gridDimSqr - gridDim; i < gridDimSqr; i++)
							percolation.Union(gridDimSqr, i);
					}
				}
				else if (state == OVER)
				{
					if (ImGui::Button("Reset Simulation"))
					{
						state = START;
						pValue = 0.0f;
						vacant.clear();
						occupied.clear();
						for (int i = 0; i < gridDimSqr; i++)
							occupied.push_back(i);
						percolation = QuickUnion(gridDimSqr + 2);
						for (int i = 0; i < gridDim; i++)
							percolation.Union(gridDimSqr + 1, i);
						for (int i = gridDimSqr - gridDim; i < gridDimSqr; i++)
							percolation.Union(gridDimSqr, i);
					}
				}
			}

			ImGui::End();
			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		}

		// Simulation Running Handler
		{
			if (state == RUNNING)
			{
				if (pValue >= 1.0f)
				{
					state = OVER;
					pValue = 0.0f;
				}
				UpdateState();
				pValue += 0.0005f;
			}
		}

		window.swapBuffers();
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}