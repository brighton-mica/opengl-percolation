#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <GL\glew.h>

class Shader 
{
	public:
		Shader();
		~Shader();

		// Creation
		void CreateFromString(const char* vertexCode, const char* fragmentCode);
		void CreateFromFiles(const char* vertexLocation, const char* fragmentLocation);
		std::string ReadFile(const char* fileLocation);

		// Use
		void UseShader();
		void ClearShader();

		// Getters
		GLuint GetProjectionLocation() { return uniformProjection; }
		GLuint GetModelLocation() { return uniformModel; }
		GLuint GetColorLocation() { return uniformColor; }

	private:
		static unsigned int numShaders;
		GLuint shaderID;
		GLuint uniformProjection, uniformModel, uniformColor;
		void CompileShader(const char* vertexCode, const char* fragmentCode);
		void AddShader(GLuint program, const char* shaderCode, GLenum shaderType);
};
