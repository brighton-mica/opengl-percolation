#pragma once
#include <GL\glew.h>
#include <iostream>

class Mesh {
public:
	Mesh();
	~Mesh();

	void CreateMesh(GLfloat* vertices, unsigned int* indices, unsigned int numOfVertices, unsigned int numOfIndices, GLenum primitive);
	void RenderMesh();
	void ClearMesh();

private:
	GLuint VAO, VBO, IBO;
	GLsizei indexCount;
	GLenum glPrimitive;
	static unsigned int meshId;
};

