#include "QuickUnion.h"

QuickUnion::QuickUnion(const int N)
{
    size = std::vector<int>(N, 1);
    for (int i = 0; i < N; i++)
        forest.push_back(i);
}

void QuickUnion::Union(const int A, const int B)
{
    if (Connected(A, B)) return;

    int rootA = Root(A);
    int rootB = Root(B);
    int& sizeA = size[rootA];
    int& sizeB = size[rootB];
    if (sizeA < sizeB)
    {
        forest[rootA] = rootB;
        sizeB += sizeA;
    }
    else
    {
        forest[rootB] = rootA;
        sizeA += sizeB;
    }
}

bool QuickUnion::Connected(const int A, const int B)
{
    return Root(A) == Root(B);
}

int QuickUnion::Root(int i)
{
    while (i != forest[i])
    {
        forest[i] = forest[forest[i]];
        i = forest[i];
    }
    return i;
}